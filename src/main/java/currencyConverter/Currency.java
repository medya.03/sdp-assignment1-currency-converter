package currencyConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Currency {
	private String name;
	private String shortName;
	private Map<String, Double> exchangeValues = new HashMap<String, Double>();
	
	// "Currency" Constructor
	public Currency(String nameValue, String shortNameValue, Map<String, Double> exchangeValues) {
		this.name = nameValue;
		this.shortName = shortNameValue;
		this.exchangeValues = exchangeValues;
	}
	
	// Getter for name
	public String getName() {
		return this.name;
	}
	
	// Setter for name
	public void setName(String name) {
		this.name = name;
	}
	
	// Getter for shortName
	public String getShortName() {
		return this.shortName;
	}
	
	// Setter for shortName
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	// Getter for exchangeValues
	public Map<String, Double> getExchangeValues() {
		return this.exchangeValues;
	}
	
	// Setter for exchangeValues
	public void setExchangeValues(String key, Double value) {
		this.exchangeValues.put(key, value);
	}


	// Convert a currency to another

	public static Double convert(Double amount, Double exchangeValue) {
		Double price;
		price = amount * exchangeValue;
		price = Math.round(price * 100d) / 100d;
		
		return price;
	}

	public void addCurrencyRate(String name, Double value){
		getExchangeValues().put(name, value);
	}
}