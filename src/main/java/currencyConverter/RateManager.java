package currencyConverter;

import json.CurrencyReader;
import json.CurrencyWriter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RateManager {
    private static RateManager instance;
    private static CurrencyReader currencyReader = CurrencyReader.getInstance();
    private static CurrencyWriter currencyWriter = CurrencyWriter.getInstance();
    private ArrayList<Currency> currencies = currencyReader.readCurrency();

    private RateManager(){}

    public static RateManager getInstance(){
        if(instance==null){
            return new RateManager();
        }
        return  instance;
    }

    public HashMap<String, String> getCurrenciesName(){
        HashMap<String, String> currenciesName = new HashMap<>();
        for(Currency currency: currencies){
            currenciesName.put(currency.getName(), currency.getShortName());
        }
        return currenciesName;
    }

    public Double getRate(String currency1, String currency2){
        Double rate;
        Currency currencyFrom;
        String shortNameCurrency2;
        for(Currency currency:currencies){
            if(currency.getName().equals(currency1)){
                return currency.getExchangeValues().get(getShortName(currency2));
            }
        }
        return null;
    }

    private String getShortName(String name){
        for(Currency currency:currencies){
            if(currency.getName().equals(name)){
                return currency.getShortName();
            }
        }
        return null;
    }


    public void update(Currency currency){
        boolean found = false;
        for(int i=0;i<currencies.size();i++){
            if(currencies.get(i).getShortName().equals(currency.getShortName())){
                Map<String, Double> currenciesRate = currencies.get(i).getExchangeValues();
                for(String currencyRate: currenciesRate.keySet()){
                    if(currency.getExchangeValues().containsKey(currencyRate)){
                        currenciesRate.put(currencyRate,currency.getExchangeValues().get(currencyRate));
                    }
                }
                found = true;
                break;
            }
        }
        if(!found){
            currencies.add(currency);
        }
        for(Currency currency1:currencies){
            if(currency1.getShortName().equals(currency.getShortName())){
                currency1.addCurrencyRate(currency.getShortName(), 1.0);
            }
            else{
                DecimalFormat format = new DecimalFormat("#0.00");
                currency1.addCurrencyRate(currency.getShortName(),
                        Currency.convert(1.0, currency.getExchangeValues().get(currency1.getShortName())));
            }

        }
        currencyWriter.writeCurrency(currencies);
    }

}
