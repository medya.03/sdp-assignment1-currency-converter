package currencyConverter;

import json.CurrencyReader;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.util.HashMap;
import java.util.ResourceBundle;

public class AddCurrencyWindow extends JFrame {
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("localization.translation"); //$NON-NLS-1$
    private JPanel contentPane;
    private JTextField newCurrencyNameField;
    private JTextField newCurrencyShortNameField;
    private JTextField currencyRate;
    private RateManager rateManager = RateManager.getInstance();
    private HashMap<String, String> currenciesName = rateManager.getCurrenciesName();
    private ArrayList<Currency> currencies = new ArrayList<>();
    private HashMap<String, Double> currenciesRates = new HashMap<>();

    private static AddCurrencyWindow addCurrencyWindow = null;


    /**
     * Create the addCurrencyWindow frame
     */
    public AddCurrencyWindow() {
        setTitle(BUNDLE.getString("AddCurrencyWindow.title.text")); //$NON-NLS-1$
        setBounds(100, 100, 589, 300);
        setLocationRelativeTo(null);
        setResizable(false);

        // Window components
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        // Label "New currency name"
        JLabel newCurrencyName = new JLabel(BUNDLE.getString("AddCurrencyWindow.lblNewCurrencyName.text")); //$NON-NLS-1$
        newCurrencyName.setHorizontalAlignment(SwingConstants.RIGHT);
        newCurrencyName.setBounds(0, 14, 92, 15);
        contentPane.add(newCurrencyName);
        //Textfield new currency name
        newCurrencyNameField = new JTextField();
        newCurrencyNameField.setBounds(100, 14, 103, 29);
        contentPane.add(newCurrencyNameField);
        newCurrencyNameField.setColumns(10);
        newCurrencyNameField.setDocument(new JTextFieldLimit(256));

        // Label "New currency short name"
        JLabel newCurrencyShortName = new JLabel(BUNDLE.getString("AddCurrencyWindow.lblNewCurrencyShortName.text")); //$NON-NLS-1$
        newCurrencyShortName.setHorizontalAlignment(SwingConstants.RIGHT);
        newCurrencyShortName.setBounds(0, 50, 92, 15);
        contentPane.add(newCurrencyShortName);
        //Textfield new currency short name
        newCurrencyShortNameField = new JTextField();
        newCurrencyShortNameField.setBounds(100, 45, 103, 29);
        contentPane.add(newCurrencyShortNameField);
        newCurrencyShortNameField.setColumns(10);
        newCurrencyShortNameField.setDocument(new JTextFieldLimit(256));


        final JComboBox<String> currenciesCombo = new JComboBox<String>();
        currenciesCombo.setBounds(100, 86, 240, 28);
        populate(currenciesCombo, currenciesName);
        contentPane.add(currenciesCombo);

        // Label "rate"
        JLabel rateLabel = new JLabel(BUNDLE.getString("AddCurrencyWindow.lblAddRate.text")); //$NON-NLS-1$
        rateLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        rateLabel.setBounds(0, 118, 92, 15);
        contentPane.add(rateLabel);
        //Textfield rate
        currencyRate = new JTextField();
        currencyRate.setText("0.00");
        currencyRate.setBounds(100, 118, 103, 29);
        contentPane.add(currencyRate);
        currencyRate.setColumns(10);
        currencyRate.setDocument(new JTextFieldLimit(256));

        //Button to add new Currency rate
        JButton btnAddRate = new JButton(BUNDLE.getString("AddCurrencyWindow.btnAddRate.text")); //$NON-NLS-1$
        btnAddRate.setBounds(205, 118, 103, 29);
        btnAddRate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String nameCurrency = currenciesCombo.getSelectedItem().toString();
                String formattedPrice;
                Double price;
                Double amount = 0.0;
                DecimalFormat format = new DecimalFormat("#0.00");
                try {
                    amount = Double.parseDouble( currencyRate.getText() ) ;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    amount = 0.0;
                }

                // Format numbers to avoid "E7" problem
                formattedPrice = format.format(amount);
                currenciesRates.put(nameCurrency, amount);

            }
        });
        contentPane.add(btnAddRate);


        //Button to save changes
        JButton save = new JButton(BUNDLE.getString("AddCurrencyWindow.btnSave.text")); //$NON-NLS-1$
        save.setBounds(100, 160, 103, 29);
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String name = newCurrencyNameField.getText();
                String shorName = newCurrencyShortNameField.getText();
                Currency currency = new Currency(name, shorName, currenciesRates);
                rateManager.update(currency);
                currenciesName = rateManager.getCurrenciesName();
            }
        });
        contentPane.add(save);
    }

    // Fill comboBox with currencies name
    public static void populate(JComboBox<String> comboBox, HashMap<String, String> currenciesName) {
        for (String currencyName:currenciesName.values()) {
            comboBox.addItem(currencyName);
        }
    }

    public static AddCurrencyWindow getInstance(){
        if(addCurrencyWindow==null) return new AddCurrencyWindow();
        return addCurrencyWindow;
    }
}