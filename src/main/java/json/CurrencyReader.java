package json;

import currencyConverter.Currency;
import org.json.JSONObject;
import org.json.JSONWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CurrencyReader {

    private static CurrencyReader instance;
    private CurrencyReader(){};
    public static CurrencyReader getInstance(){
        if(instance==null) return new CurrencyReader();
        return instance;
    }

    private File file;
    private FileReader fileReader;
    private BufferedReader bufferedReader;
    private JSONWriter jsonWriter;



    public ArrayList<Currency> readCurrency(){
        ArrayList<Currency> currencies = new ArrayList<>();
        try{
            file = new File("src/main/resources/currency.json");
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String str;
            String jsonStr = "";
            while ((str =bufferedReader.readLine()) != null){
                jsonStr+=str;
            }

            JSONObject currenciesJson = new JSONObject(jsonStr);

            for(String currencyCode:currenciesJson.keySet()){
                JSONObject currencyJson = currenciesJson.getJSONObject(currencyCode);
                currencies.add(new Currency(currencyJson.getString("name"), currencyCode,
                        checkAndTransform(currencyJson.getJSONObject("rates").toMap())));
            }


        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return currencies;
    }

    private Map<String, Double> checkAndTransform(Map<String, Object> inputMap) {
        Map<String, Double> result = new HashMap<>();
        for (Map.Entry<String, Object> entry : inputMap.entrySet()) {
            try {
                result.put(entry.getKey(), Double.parseDouble(entry.getValue().toString()) );
            } catch (ClassCastException e) {
                throw e; // or a required error handling
            }
        }
        return result;
    }
}
