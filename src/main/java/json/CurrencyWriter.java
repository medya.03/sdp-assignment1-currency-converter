package json;

import currencyConverter.Currency;
import org.json.JSONObject;
import org.json.JSONWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CurrencyWriter {
    private static CurrencyWriter instance;
    private CurrencyWriter(){}

    public static CurrencyWriter getInstance(){
        if(instance == null) return new CurrencyWriter();
        return instance;
    }

    public void writeCurrency(ArrayList<Currency> currencies){
        try{
            File file = new File("src/main/resources/currency.json");
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            JSONWriter jsonWriter = new JSONWriter(bufferedWriter);
            jsonWriter.object();
            for(Currency currency: currencies){
                JSONObject currencyInfo = new JSONObject();
                currencyInfo.put("name", currency.getName());
                currencyInfo.put("rates", currency.getExchangeValues());
                jsonWriter.key(currency.getShortName()).value(currencyInfo);
            }
            jsonWriter.endObject();
            try {
                bufferedWriter.close();
                fileWriter.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
